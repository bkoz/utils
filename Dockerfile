#
# Utility image for Summit 2020 lab.
#
FROM quay.io/podman/stable
LABEL maintainer="bkozdemba at gmail dot com"

#
# Install packages
#
# RUN dnf -y update; dnf -y clean all
RUN dnf -y install iputils e2fsprogs --setopt install_weak_deps=false; dnf -y clean all
#
# USER 1001
CMD [ "/usr/bin/echo",  "Utility image just ran." ]
